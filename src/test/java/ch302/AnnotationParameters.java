package ch302;

import org.testng.annotations.Test;
import org.testng.annotations.Parameters;

public class AnnotationParameters {
    @Parameters({ "repoName" })
    @Test
    public void testparameter(String repo_Name) {
    	System.out.println("Invoked method testparameter in " + repo_Name);
    }
}