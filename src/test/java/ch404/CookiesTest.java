package ch404;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Cookie;
import java.util.Set;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CookiesTest {
	@Test
	public void addCookie() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/blank.html");
			// Add cookie into current browser context
			driver.manage().addCookie(new Cookie("key", "value"));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void getNamedCookie() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/blank.html");
			// Add cookie into current browser context
			driver.manage().addCookie(new Cookie("foo", "bar"));
			// Get cookie details with named cookie 'foo'
			Cookie cookie = driver.manage().getCookieNamed("foo");
			Assert.assertEquals(cookie.getValue(), "bar");
		} finally {
			driver.quit();
		}
	}

	@Test
	public void getAllCookies() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/blank.html");
			// Add cookies into current browser context
			driver.manage().addCookie(new Cookie("test1", "cookie1"));
			driver.manage().addCookie(new Cookie("test2", "cookie2"));
			// Get cookies
			Set<Cookie> cookies = driver.manage().getCookies();
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("test1")) {
					Assert.assertEquals(cookie.getValue(), "cookie1");
				}

				if (cookie.getName().equals("test2")) {
					Assert.assertEquals(cookie.getValue(), "cookie2");
				}
			}
		} finally {
			driver.quit();
		}
	}

	@Test
	public void deleteCookieNamed() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/blank.html");
			driver.manage().addCookie(new Cookie("test1", "cookie1"));
			// delete cookie named
			driver.manage().deleteCookieNamed("test1");
		} finally {
			driver.quit();
		}
	}

	@Test
	public void deleteCookieObject() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/blank.html");
			Cookie cookie = new Cookie("test2", "cookie2");
			driver.manage().addCookie(cookie);
			/*
			 * Selenium Java bindings also provides a way to delete cookie by passing cookie
			 * object of current browsing context
			 */
			driver.manage().deleteCookie(cookie);
		} finally {
			driver.quit();
		}
	}

	@Test
	public void deleteAllCookies() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/blank.html");
			// Add cookies into current browser context
			driver.manage().addCookie(new Cookie("test1", "cookie1"));
			driver.manage().addCookie(new Cookie("test2", "cookie2"));
			// Delete All cookies
			driver.manage().deleteAllCookies();
		} finally {
			driver.quit();
		}
	}
}
