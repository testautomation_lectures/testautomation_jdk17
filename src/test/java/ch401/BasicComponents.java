package ch401;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;

public class BasicComponents {
  
	@Test
	public void hitSelenium() {
		
		/* Start WebDriver session */
        WebDriver driver = new ChromeDriver();

		/* Do Browser action : navigate to specific site  */
        driver.get("https://www.selenium.dev/selenium/web/web-form.html");

		/* Take a browser info :  get the title of current site  */
        String pageTitle = driver.getTitle();
		Assert.assertEquals(pageTitle, "Web form");
		System.out.println("Page Title = " + pageTitle);

		/* Set Wait strategy  : set wait time for each actions  */
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(500));

		/* Find an WebElement : find element(s)  */
        WebElement textBox = driver.findElement(By.name("my-text"));
        WebElement submitButton = driver.findElement(By.cssSelector("button"));

		/* Do WebElement action : type some letters  */
        textBox.sendKeys("Selenium");
		/* Do WebElement action : click a button  */
        submitButton.click();

		/* Find an WebElement : find element(s)  */
        WebElement message = driver.findElement(By.id("message"));
		/* Do WebElement action : read text  */
        String value = message.getText();
        Assert.assertEquals("Received!", value);

		/* Close WebDriver session */
		driver.quit();
		/* cmd /c taskkill /f /im chromedriver.exe  */
	}	
}