# TestAutomation_JDK17

## Objective

테스트 자동화 시작반 - 소스 공유장소(JDK17용).

## Folder schema

|   Folders            |         Comments                        |
|---------------------|-----------------------------------------|
| /src/test/java       | Test scripts                            |
| /src/test/resources | Test configuration of execution         |


## Change log (reverse chronological order)
***

### 0.3.0 (May 8, 2024)
> Added Selenium examples

### 0.2.0 (Nov 13, 2023)
> Added TestNG examples

### 0.1.0 (Jun 16, 2023)
> initial checked in,  Creating a single test with TestNG